import requests
import re
from urllib.parse import urlparse
import tldextract
final_url_list=[]
site_url='https://www.shubhloans.com'

'''
documentation :-
    AIM:- getting all unique url from given website except another domain url
    approach:- 1) get the all url from  the given (url ex:-https://success4.us/) html page 
               2) itret all the base page url and  find url linked page url if there is new url then again  itret on those url 
               and the url and so on. like that get all the url from the hole website.
               3)make the listed url in to (unique)
    AIM:- extract the all  images from unique url list
    if we get the all images make it unique 
    give the out put in dict format
    
'''

def get_html(url):
    '''

    :param url: given url(site_url)
    :return: get all html page data
    '''
    try:
        html = requests.get(url)
    except Exception as e:
        print(e)
        return ""
    return html.content.decode('latin-1')


def get_links(url):
    '''

    :param url:taking url as param
    :return:all url of the html
    '''
    html = get_html(url)
    parsed = urlparse(url)
    base = f"{parsed.scheme}://{parsed.netloc}"
    links = re.findall('''<a\s+(?:[^>]*?\s+)?href="([^"]*)"''', html)
    for i, link in enumerate(links):
        if not urlparse(link).netloc:
            link_with_base = base + link
            links[i] = link_with_base

    return set(filter(lambda x: 'mailto' not in x, links))

def get_images(url):
    '''

    :param url: taking url as parameter
    :return: all image of that page
    '''
    for i in url:
        html = get_html(i)
        image=re.findall('''<img\s+(?:[^>]*?\s+)?src="([^"]*)"''', html)
        print('\n \n\n image of particular url:- '+i)
        print({'image':set(image),'url':i})



def iterate_links(i):
    for j in get_links(i):
        # print(j)
        # checking the url  is same domain or not and url is not persent in the final list url
        if tldextract.extract(j).domain + '.' + tldextract.extract(j).suffix == tldextract.extract(
                site_url).domain + '.' + tldextract.extract(site_url).suffix and j not in final_url_list:
            final_url_list.append(i)
            # break
            # recursively colling the  function
            iterate_links(j)

        else:
            # if url is out of domain and  or same url already exist in the final list url
            pass




if __name__ == "__main__":
    all_links = get_links(site_url)
    print('please wait it will take some time..................')
    for i in all_links:


        iterate_links(i)

    data=set(final_url_list)
    print('\n \n\n all  unique url')
    get_images(data)
    print('###########################################################\n \n')
    print(len(data), 'total unique url of the website')
